#[path="Config.rs"]
mod Config;

use actix_web::
{
	http::StatusCode, get, web, App, HttpServer, Responder, HttpResponse
};


use askama::Template;

use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

use self::Config::TOML_Value;

#[derive(askama::Template)]
#[template(path = "Index.ahtml")]
struct Index_Template { }

#[derive(askama::Template)]
#[template(path = "Exceptions/Not_Found.ahtml")]
struct Not_Found_Template { }


#[derive(askama::Template)]
#[template(path = "App/Offline.ahtml")]
struct Offline_Template { }


#[get("/")]
async fn Index() -> impl Responder
{

	HttpResponse::build(StatusCode::OK)
		.content_type("text/html; charset=utf-8")
		.append_header(("Cache-Control", "max-age=150"))
		.append_header(("X-Content-Type-Options", "nosniff"))
		.append_header(("Content-Security-Policy", "script-src 'self'; default-src 'self'"))
		.body(Index_Template {}.render().unwrap())
}


#[get("/CSS/Main")]
async fn Main_CSS() -> impl Responder
{
	HttpResponse::build(StatusCode::OK)
		.content_type("text/css; charset=utf-8")
		.append_header(("Cache-Control", "max-age=31536000, immutable"))
		.append_header(("X-Content-Type-Options", "nosniff"))
		.body(include_str!("Public/CSS/Main.css"))
}

async fn Not_Found() -> impl Responder
{
	HttpResponse::build(StatusCode::NOT_FOUND)
		.content_type("text/html; charset=utf-8")
		.append_header(("Cache-Control", "max-age=31536000, immutable"))
		.append_header(("X-Content-Type-Options", "nosniff"))
		.append_header(("Content-Security-Policy", "script-src 'self'; default-src 'self'"))
		.body(Not_Found_Template {}.render().unwrap())
}

#[get("/App/Offline")]
async fn Offline() -> impl Responder
{

	HttpResponse::build(StatusCode::OK)
		.content_type("text/html; charset=utf-8")
		.append_header(("Cache-Control", "max-age=150"))
		.append_header(("X-Content-Type-Options", "nosniff"))
		.append_header(("Content-Security-Policy", "script-src 'self'; default-src 'self'"))
		.body(Offline_Template {}.render().unwrap())
}


#[get("/Media/Images/Logos/Foxhole")]
async fn Foxhole_Logo() -> impl Responder
{
	HttpResponse::build(StatusCode::OK)
		.content_type("image/svg+xml; charset=utf-8")
		.append_header(("Cache-Control", "max-age=31536000, immutable"))
		.append_header(("X-Content-Type-Options", "nosniff"))
		.append_header(("Content-Security-Policy", "script-src 'self'; default-src 'self'"))
		.body(include_str!("Public/Static/Media/Images/Logos/Foxhole.svg"))
}

#[get("/Manifest")]
async fn Manifest() -> impl Responder
{

	HttpResponse::build(StatusCode::OK)
		.content_type("application/manifest+json; charset=utf-8")
		.append_header(("Cache-Control", "max-age=31536000, immutable"))
		.append_header(("X-Content-Type-Options", "nosniff"))
		.append_header(("Content-Security-Policy", "script-src 'self'; default-src 'self'"))
		.body(include_str!("Public/JSON/Manifest.json"))
}

#[get("/Service_Worker")]
async fn Service_Worker() -> impl Responder
{
	HttpResponse::build(StatusCode::OK)
		.content_type("text/javascript; charset=utf-8")
		.append_header(("Cache-Control", "max-age=31536000, immutable"))
		.append_header(("X-Content-Type-Options", "nosniff"))
		.append_header(("Content-Security-Policy", "script-src 'self'; default-src 'self'"))
		.body(include_str!("Public/JS/Service_Worker.js"))
}


#[actix_web::main]
pub async fn Start() -> std::io::Result<()>
{

	let mut server = HttpServer::new
	(|| {
		App::new()
			.default_service(web::to(Not_Found))
			.service(Index)
			.service(Main_CSS)
			.service(Foxhole_Logo)
			.service(Manifest)
			.service(Service_Worker)
			.service(Offline)
	}).bind
	(
		std::net::SocketAddrV6::new
		(
			std::net::Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0),
			8080, 0, 0
		)
	)?;



	if let Some(value) = Config::Config.get("Certificate")
	{
		if let TOML_Value::table(table) = value
		{
			if let TOML_Value::string(key) = &table["Certificate_Key_Path"]
			{
				if let TOML_Value::string(chain) = &table["Certificate_Chain_Path"]
				{
					let mut builder: openssl::ssl::SslAcceptorBuilder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
					builder
						.set_private_key_file(key, SslFiletype::PEM)
						.unwrap();
					builder.set_certificate_chain_file(chain).unwrap();

					server = server.bind_openssl
					(
						std::net::SocketAddrV6::new
						(
							std::net::Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0),
							8443, 0, 0
						),
						builder
					)?;
				}
			}
		}
	}

	server
		.run()
		.await
}
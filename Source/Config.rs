use std::io::Write;
use chrono::TimeZone;


struct Attributes
{
	required: bool
}


enum Item<'a>
{
	key(&'a str, Attributes),
	table
	(
		&'a str,
		bool, /* Is an inline table */
		&'a [Item<'a>],
		Attributes
	)
}


const Config_Template: &[Item] =
&[
	Item::table
	(
		"Certificate",
		false,
		&[
			Item::key("Certificate_Chain_Path", Attributes { required: true }),
			Item::key("Certificate_Key_Path", Attributes { required: true }),
		],
		Attributes { required: false }
	)
];


#[derive(Clone)]
pub enum TOML_Value
{
	string(std::string::String),
	i64(i64),
	f64(f64),
	bool(bool),
	datetime(chrono::DateTime<chrono::FixedOffset>),
	array(Vec<TOML_Value>),
	table(std::collections::HashMap<std::string::String, TOML_Value>)
}

fn Confirm(default: bool) -> bool
{
	let mut input: std::string::String = "".to_string();
	loop
	{
		std::io::stdin()
			.read_line(&mut input)
			.expect("Failed to read input");


		input = input.trim().to_lowercase();


		if input == "" { return default; }

		if input == "yes" || input == "y" { return true; }

		if input == "no" || input == "n" { return false; }

		print!("Sorry, \"{}\" was not understood.\n\
			[\x1b[92mYes\x1b[0m\
			/\
			\x1b[91mNo\x1b[0m]", input);
		std::io::stdout().flush().unwrap();
		input.clear();
	}
}

fn Get_New_Values(items: &[Item], parents: std::string::String) -> (std::string::String, toml::Value)
{
	let mut output: (std::string::String, toml::map::Map<std::string::String, toml::Value>) =
	(
		"".to_string(),
		toml::map::Map::new()
	);

	for item in items
	{
		match item
		{
			Item::key(key, attributes) =>
			{
				let mut input: std::string::String = "".to_string();

				loop
				{
					let mut note: std::string::String = "".to_string();
					
					if attributes.required { note = " (Required)".to_string() };

					println!("Set value for \"{}{}\"{}", parents, key, note);
					
					std::io::stdin()
						.read_line(&mut input)
						.expect("Failed to read input");
					
					input.truncate(input.len() - 1usize);

					let toml: std::string::String = format!("{} = {}\n", key, input);

					if !attributes.required || input != "".to_string()
					{
						output.0.push_str(toml.as_str());
						if let toml::Value::Table(table) = toml.parse::<toml::Value>().unwrap()
						{
							output.1.insert(key.to_string(), (&table[&key.to_string()]).to_owned());
						};
						
						break;
					}
				}


			},
			Item::table(key, is_inline, items, attributes) =>
			{
				if !attributes.required
				{
					print!("\nAdd table {}{}? \
					(\x1b[92mYes\x1b[0m\
					/\
					\x1b[91mNo\x1b[0m)[Y]", parents, key);

					std::io::stdout().flush().unwrap();

					if !Confirm(true)
					{
						continue;
					}
				}

				let mut table_output: (std::string::String, toml::Value) = Get_New_Values(items, format!("{}{}.", parents, key));

				table_output.0 = if *is_inline
				{
					format!("{} = {{ {} }}", key, table_output.0.replace("\n", ", "))
				}
				else
				{
					format!("[{}]\n{}", key, table_output.0)
				};

				output.0.push_str(table_output.0.as_str());
				output.1.insert(key.to_string(), table_output.1);
			}
		}
	}

	(output.0, toml::Value::Table(output.1))
}


fn Generate_New_Config
(
	config_file_path: &std::path::PathBuf
) -> toml::Value
{
	print!("\nWould you like to create a new Config file? \
		(\x1b[92mYes\x1b[0m\
		/\
	\x1b[91mNo\x1b[0m)[Y]");

	std::io::stdout().flush().unwrap();

	if !Confirm(true) { std::process::exit(-1); }
	
	let config_dir = config_file_path.parent().unwrap();
	
	if !config_dir.exists()
	{
		std::fs::create_dir(config_dir)
			.expect(
				format!(
					"Could not create directory: \"{}\"", 
					config_dir.display()).as_str());
	}

	let new_config: (std::string::String, toml::Value) = Get_New_Values(Config_Template, "".to_string());

	let mut config_file: std::fs::File = match std::fs::File::create(&config_file_path)
	{
        Err(error) => panic!("Could not create file: \"{}\": {}", config_file_path.display(), error),
        Ok(config_file) => config_file,
    };

	config_file.write(new_config.0.as_bytes())
		.expect("Failed to write to config.");

	new_config.1
}



fn Get_TOML_Values(value: toml::Value) -> TOML_Value
{
	return match value
	{
		toml::Value::String(string) => TOML_Value::string(string),
		toml::Value::Integer(int) => TOML_Value::i64(int),
		toml::Value::Float(float) => TOML_Value::f64(float),
		toml::Value::Boolean(bool) => TOML_Value::bool(bool),
		toml::Value::Datetime(datetime) =>
		{
			let mut offset_sec: i32 = 0;

			if let Some(offset) = datetime.offset
			{
				offset_sec = match offset
				{
					toml::value::Offset::Z => 0,
					toml::value::Offset::Custom { hours, minutes } =>
						(<i8 as Into<i32>>::into(hours) * 3600) +
						(<u8 as Into<i32>>::into(minutes) * 60)
				}
			}

			let mut date: toml::value::Date = toml::value::Date { year: 0, month: 0, day: 0 };

			let mut time: toml::value::Time = toml::value::Time { hour: 0, minute: 0, second: 0, nanosecond: 0 };

			if let Some(toml_date) = datetime.date { date = toml_date; };

			if let Some(toml_time) = datetime.time { time = toml_time; };

			TOML_Value::datetime
			(
				chrono::FixedOffset::east(offset_sec)
					.ymd(
						date.year.into(),
						date.month.into(),
						date.day.into()
					)
					.and_hms_nano(
						time.hour.into(),
						time.minute.into(),
						time.second.into(),
						time.nanosecond.into()
					)
			)
		},
		toml::Value::Array(array) =>
		{
			let mut values: Vec<TOML_Value> = vec![];

			for value in array
			{
				values.push(Get_TOML_Values(value));
			}

			TOML_Value::array(values)
		},
		toml::Value::Table(table) => 
		{
			let mut toml_table: std::collections::HashMap<std::string::String, TOML_Value> =
				std::collections::HashMap::new();

			for (key, value) in table
			{
				toml_table.insert(key, Get_TOML_Values(value));
			}

			TOML_Value::table(toml_table)
		},
	}
}


fn Apply_Attributes
(
	items: &[Item],
	config: &std::collections::HashMap<std::string::String, TOML_Value>,
	parents: std::string::String
) -> Result<(), std::io::Error>
{
	for item in items
	{
		match item
		{
			Item::key(key, attributes) =>
			{
				if config.contains_key(&key.to_string())
				{
					match config[&key.to_string()]
					{
						TOML_Value::table(_) =>
						{
							return Err
							(
								std::io::Error::new
								(
									std::io::ErrorKind::InvalidData,
									format!("\"{}{}\" should not be a table...", parents, key)
								)
							);
						}
						_ => {}
					};
				}
				else
				{
					if attributes.required
					{
						return Err
						(
							std::io::Error::new
							(
								std::io::ErrorKind::InvalidData,
								format!("Key \"{}{}\" was required but it does not exist...", parents, key)
							)
						)
					}
				}
			},
			Item::table(key, _, items, attributes) =>
			{
				if !config.contains_key(&key.to_string())
				{
					if attributes.required
					{
						return Err
						(
							std::io::Error::new
							(
								std::io::ErrorKind::InvalidData,
								format!("Table \"{}{}\" is required, but it does not exist...", parents, key)
							)
						);
					}
				}
				else
				{
					if let TOML_Value::table(table) = &config[&key.to_string()]
					{
						return Apply_Attributes(items, table, format!("{}{}.", parents, key));
					}
					else
					{
						return Err
						(
							std::io::Error::new
							(
								std::io::ErrorKind::InvalidData,
								format!("\"{}{}\" should be a table...", parents, key)
							)
						);
					}
				
				}

			}
		};
	};
	Ok(())
}


lazy_static!
{
	pub static ref Config: std::collections::HashMap<std::string::String, TOML_Value> =
	{
		let config_file_path: std::path::PathBuf = platform_dirs::AppDirs::new(Some("Foxhole/Config.toml"), false)
		.unwrap()
		.config_dir;

		let mut config_contents: toml::Value = 
			match std::fs::read_to_string(&config_file_path)
			{
				Ok(contents) => contents
					.parse::<toml::Value>()
					.expect("Invalid toml."),
				Err(error) => 
				{
					println!("\x1b[91mWarning:\x1b[0m Failed to read config file at \"{}\": {}", config_file_path.display(), error);

					Generate_New_Config(&config_file_path)
				}
			};


		loop
		{
			let mut config: std::collections::HashMap<std::string::String, TOML_Value> =
				std::collections::HashMap::new();



			for (key, value) in config_contents.as_table().unwrap()
			{
				config.insert(key.to_string(), Get_TOML_Values(value.to_owned()));
			}
			

			match Apply_Attributes(Config_Template, &config.clone(), "".to_string())
			{
				Ok(_) => { return config; },
				Err(error) =>
				{
					println!("\x1b[91mError:\x1b[0m {}", error);
					config_contents = Generate_New_Config(&config_file_path);
				}
			}

		}
	};


}
#![allow(nonstandard_style)]

#[macro_use]
extern crate lazy_static;

mod Web_App;

fn main() -> std::io::Result<()>
{
	Web_App::Start()
}
